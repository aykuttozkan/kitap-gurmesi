# Kitap Gurmesi
Kitaplar hakkında yorum yapabileceğiniz ve fikir alabileceğiniz bir site projesi.


### Alt Yapı Bilgileri

Proje .net core 7 mvc alt yapısı ile hazırlanmıştır.

Veritabanı olarak postgresql tercih edilmiştir.

### Projenin Çalıştırılması

- [ ] Öncelikle bilgisayarınızda _**.net 7**_ yüklü olmalıdır. Eğer yüklü değilse yüklemek için [**Tıklayınız**.](https://dotnet.microsoft.com/en-us/download)
- [ ] Projenin bir veritabanı bağlantısına ihtiyaç olduğundan ve proje postgreql db üzerine inşa edildiğinden bilgisayarınızda **Postgresql** kurulu olmalıdır.
- [ ] Son olarak projeyi derleyip çalıştırmak için yine bilgisayarınızda visual studio, visual studio code, rider benzer ide ve editör araçların olması tavsiye edilmektedir.
> NOT! Eğer bilgisayarınızda docker yüklü ise postgresql container'ı oluşturmanız yeterli olacaktır.

### Docker Üzerinden Postgreql Veritabanı Oluşturma
Projenin ihtiyacı olan veritabanını oluşturabilmek için aşağıdaki komutu çalıştırmanız yeterli olacaktır.
```
docker run --name techjobpgsql -e POSTGRES_PASSWORD=mysecretpassword -p 5435:5432 -d postgres:alpine
```

### Logların Takip Edilmesi
Proje içerisinde Serilog kütüphanesi eklenmiş olup, aşağıdaki url ile log takip ekranına ulaşabilirsiniz.

> https://localhost:7189/serilog-ui/index.html#logs

### Ef Core Db Table Oluşturma Adımı

* Migration dosyasını üretmek için src klasörü altında aşağıdaki komutu çalıştırınız.

```
dotnet ef migrations add --project KitapGurmesi.Web/KitapGurmesi.Web.csproj --startup-project KitapGurmesi.Web/KitapGurmesi.Web.csproj --context KitapGurmesi.Web.Models.Contexts.ApplicationDbContext --configuration Debug Initialize --output-dir Migrations
```

> **NOT!** Yukarıdaki komutu sadece yeni bir migration dosyası oluşturmak istediğinizde kullanınız. Aksi halde varsayılan olarak gerekili olan migration dosyası proje içerisinde mevcuttur.



* Migration dosyasının db'ye yansıması için aşağıdaki komutu src klasörü altında çalıştırınız.

```
dotnet ef database update --project KitapGurmesi.Web/KitapGurmesi.Web.csproj --startup-project KitapGurmesi.Web/KitapGurmesi.Web.csproj --context ApplicationDbContext -- --environment Development
```

> **NOT!** Proje içerisinde auto migration özelliği aktif edildiğinden projeyi çalıştırırken tüm db operasyonları gerçekleştirilecektir.


> **NOT!** Proje görselleri **_screenshots_** klasörü altında bulunmaktadır. Projeye ait tanıdım videosu **_video_** klasörü altında bulunmaktadır.