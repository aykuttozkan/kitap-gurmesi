using KitapGurmesi.Web.Models.Entities;

namespace KitapGurmesi.Web.ViewModels;

public record BookListByCategoryVM
{
    public string CategoryName { get; set; } = string.Empty;
    
    public List<BookAndAuthor> BookAndAuthors { get; set; }
}

public record BookAndAuthor
{
    public Book Book { get; set; }

    public Author Author { get; set; }
}

public record CommentListByAppUserIdVM
{
    public string CategoryName { get; set; } = string.Empty;

    public string BookGuid { get; set; }
        
    public Comment Comment { get; set;}
}