namespace KitapGurmesi.Web.ViewModels;

public class BookCommentAddVM
{
    public string BookGuid { get; set; }
    
    public string CommentHeader { get; set; }

    public string CommentStr { get; set; }
}