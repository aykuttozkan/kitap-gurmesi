using System.ComponentModel.DataAnnotations;

namespace KitapGurmesi.Web.ViewModels;

public record BookAddVM
{
    [Required(ErrorMessage = "Kitap adı boş geçilemez.")]
    public string Name { get; set; }

    [Required(ErrorMessage = "Kitap açıklama alanı boş geçilemez.")]
    public string Description { get; set; } = string.Empty;

    [Required(ErrorMessage = "EanCode kodu boş geçilemez.")]
    public string EanCode { get; set; }
    
    [Required(ErrorMessage = "Yazar seçimini boş bırakamazsınız.")]
    public string AuthorGuid { get; set; }
    
    [Required(ErrorMessage = "Kategori seçimini boş bırakamazsınız.")]
    public IList<string>? SelectedCategories { get; set; }
}