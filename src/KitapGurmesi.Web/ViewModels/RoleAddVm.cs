using System.ComponentModel.DataAnnotations;

namespace KitapGurmesi.Web.ViewModels;

public record RoleAddVm([Required(ErrorMessage = "Rol bilgisi boş geçilemez")]string Name);