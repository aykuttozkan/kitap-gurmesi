using System.ComponentModel.DataAnnotations;

namespace KitapGurmesi.Web.ViewModels;

public record UserEditVM
{
    [Required(ErrorMessage = "Kullanıcı Id bilgisi hatalı.")]
    public string Id { get; set; } = string.Empty;
    
    [Required(ErrorMessage = "Kullanıcı adı boş geçilemez")]
    public string UserName { get; set; }

    [Required(ErrorMessage = "Email bilgisi boş geçilemez")]
    [EmailAddress(ErrorMessage = "Geçersiz email bilgisi.")]
    public string Email { get; set; }
    
    [DataType(DataType.Password)]
    public string? Password { get; set; }

    public IList<string>? SelectedRoles { get; set; }
}