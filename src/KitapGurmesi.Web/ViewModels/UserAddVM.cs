using System.ComponentModel.DataAnnotations;

namespace KitapGurmesi.Web.ViewModels;

public record UserAddVM
{   
    [Required(ErrorMessage = "Kullanıcı adı boş geçilemez.")]
    public string UserName { get; set; }

    [Required(ErrorMessage = "Email bilgisi boş geçilemez.")]
    [EmailAddress(ErrorMessage = "Geçersiz email bilgisi.")]
    public string Email { get; set; }
    
    [Required(ErrorMessage = "Parola bilgisi boş geçilemez.")]
    [DataType(DataType.Password)]
    public string Password { get; set; }

    [Required(ErrorMessage = "Lütfen parolanızı doğrulamak için parolanızı tekrar giriniz.")]
    [DataType(DataType.Password)]
    [Compare(nameof(Password), ErrorMessage = "Girdiğiniz parolalar eşleşmiyor.")]
    public string ComparePassword { get; set; }
}