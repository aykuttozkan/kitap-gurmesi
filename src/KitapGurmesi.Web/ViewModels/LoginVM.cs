using System.ComponentModel.DataAnnotations;

namespace KitapGurmesi.Web.ViewModels;

public record LoginVM
{
    [Required(ErrorMessage = "Kullanıcı adınız boş geçilemez")]
    public string UserName { get; set; }
    
    [Required(ErrorMessage = "Parola bilgisi boş geçilemez.")]
    [DataType(DataType.Password)]
    public string Password { get; set; }
}