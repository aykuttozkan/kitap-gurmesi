using KitapGurmesi.Web.Models.Contexts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.ViewComponents;

public class CategoryViewComponent : ViewComponent
{
    private readonly ApplicationDbContext _dbContext;
    
    public CategoryViewComponent(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    // http-s://stackoverflow.com/questions/69245954/i-get-this-error-when-i-use-view-component
    public async Task<IViewComponentResult> InvokeAsync()
    {
        var categoryListResult = await _dbContext.Categories.OrderBy(o => o.Id).ToListAsync();
        
        return View("~/Views/Shared/Components/Books/Category.cshtml", categoryListResult);
    }
}