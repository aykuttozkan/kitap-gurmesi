using KitapGurmesi.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace KitapGurmesi.Web.TagHelpers;

[HtmlTargetElement("td", Attributes = "asp-role-users")]
public class RoleUsersTagHelper : TagHelper
{
    [HtmlAttributeName("asp-role-users")]
    public string RoleId { get; set; } = null!;

    private readonly RoleManager<AppRole> _roleManager;
    private readonly UserManager<AppUser> _userManager;
    
    public RoleUsersTagHelper(RoleManager<AppRole> roleManager, UserManager<AppUser> userManager)
    {
        _roleManager = roleManager;
        _userManager = userManager;
    }
    public override async Task ProcessAsync(TagHelperContext context, TagHelperOutput output)
    {
        try
        {
            List<string> userNameList = new();

            var roleResult = await _roleManager.FindByIdAsync(RoleId);

             if (roleResult != null && roleResult.Name != null)
             {
                 foreach (var user in await _userManager.Users.ToListAsync())    
                 {
                     if (await _userManager.IsInRoleAsync(user, roleResult.Name))
                     {
                         userNameList.Add(user.UserName);
                     }
                 }
            
                 output.Content.SetContent(userNameList.Count == 0
                     ? "Kullanıcı bulunamadı"
                     : string.Join(',', userNameList));
            }
        }
        catch (Exception e)
        {
            Log.Error(e, $"Tag helper işlemleri sırasında bir hata meydana geldi. {e.Message}");
        }
        
    }
}