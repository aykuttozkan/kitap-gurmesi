using System.Security.Claims;
using KitapGurmesi.Web.Models.Contexts;
using KitapGurmesi.Web.Models.Entities;
using KitapGurmesi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Controllers;

public class BookController : Controller
{
    private readonly ApplicationDbContext _dbContext;
    private readonly UserManager<AppUser> _userManager;

    public BookController(ApplicationDbContext dbContext, UserManager<AppUser> userManager)
    {
        _dbContext = dbContext;
        _userManager = userManager;
    }

    [HttpGet]
    public async Task<IActionResult> IndexWithoutCategory()
    {
        var categoryResult = await _dbContext.Categories
            .Include(i => i.Books)
            .ThenInclude(book => book.Author)
            .Select(s => new BookListByCategoryVM
            {
                CategoryName = s.Name,
                BookAndAuthors = s.Books.Select(ss => new BookAndAuthor { Book = ss, Author = ss.Author }).ToList()
            })
            .ToListAsync();

        return View(categoryResult);
    }
    
    [HttpGet]
    public async Task<IActionResult> Index(string id)
    {
        var categoryResult = await _dbContext.Categories
            .Include(i => i.Books)
            .ThenInclude(book => book.Author)
            .Where(p => p.CategoryGuid.Equals(Guid.Parse(id)))
            .Select(s => new BookListByCategoryVM
            {
                CategoryName = s.Name,
                BookAndAuthors = s.Books.Select(ss => new BookAndAuthor { Book = ss, Author = ss.Author }).ToList()
            })
            .FirstOrDefaultAsync();

        return View(categoryResult);
    }

    [HttpGet, Authorize(Roles = "Admin,Editor")]
    public async Task<IActionResult> BookAdd()
    {
        ViewBag.CategoryList = await _dbContext.Categories.ToListAsync();
        ViewBag.AuthorList = await _dbContext.Authors.ToListAsync();
        
        return View();
    }
    
    [HttpPost, Authorize(Roles = "Admin,Editor")]
    public async Task<IActionResult> BookAdd(BookAddVM model)
    {
        if (ModelState.IsValid)
        {
            var categories = await _dbContext.Categories.Where(p =>
                model.SelectedCategories.Select(s => Guid.Parse(s)).ToList().Contains(p.CategoryGuid)).ToListAsync();

            var author =
                await _dbContext.Authors.FirstOrDefaultAsync(p => p.AuthorGuid == Guid.Parse(model.AuthorGuid));

            var book = new Book
            {
                Name = model.Name,
                Description = model.Description,
                Author = author,
                EanCode = model.EanCode,
                Categories = categories,
                CreatedDate = DateTime.Now,
                CreatedUserGuid = Guid.Parse(User.Claims.FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier).Value)
            };

            _dbContext.Books.Add(book);

            await _dbContext.SaveChangesAsync();

            return RedirectToAction("Index", "Home");
        }
        
        ViewBag.CategoryList = await _dbContext.Categories.ToListAsync();
        ViewBag.AuthorList = await _dbContext.Authors.ToListAsync();
        return View(model);
    }
    
    [HttpGet]
    public async Task<IActionResult> BookDetails(string id)
    {
        var bookResult = await _dbContext.Books.Include(i => i.Author)
            .Include(c => c.Categories)
            .Include(comment => comment.Comments)
            .ThenInclude(bookAppUser => bookAppUser.AppUser)
            .FirstOrDefaultAsync(p => p.BookGuid.Equals(Guid.Parse(id)));

        return View(bookResult);
    }

    [HttpPost, Authorize]
    public async Task<IActionResult> BookCommentAdd(string id, BookCommentAddVM model)
    {
        try
        {
            if (ModelState.IsValid)
            {
                if (id != model?.BookGuid)
                    return RedirectToAction("BookDetails", new { id = model.BookGuid });

                var bookResult =
                    await _dbContext.Books.FirstOrDefaultAsync(p => p.BookGuid == Guid.Parse(model.BookGuid));

                if (bookResult is null)
                    return RedirectToAction("BookDetails", new { id = model.BookGuid });

                var book = await _dbContext.Books.FirstOrDefaultAsync(p => p.BookGuid == Guid.Parse(id));

                var user = await _userManager.FindByIdAsync(User.Claims
                    .FirstOrDefault(p => p.Type == ClaimTypes.NameIdentifier).Value);

                var comment = new Comment
                {
                    Book = book,
                    CreatedDate = DateTime.Now,
                    CommentGuid = Guid.NewGuid(),
                    CommentHeader = model.CommentHeader,
                    CommentStr = model.CommentStr,
                    AppUser = user
                };

                await _dbContext.Comments.AddAsync(comment);

                await _dbContext.SaveChangesAsync();

                return RedirectToAction("BookDetails", new { id = model.BookGuid });
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

        return RedirectToAction("BookDetails", routeValues: id);
    }

    [HttpGet, Authorize]
    public async Task<IActionResult> BookCommentDelete(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            return RedirectToAction("Index");

        var arr = id.Split("--");

        var comment = await _dbContext.Comments.FirstOrDefaultAsync(p => p.CommentGuid == Guid.Parse(arr[0]));

        _dbContext.Remove(comment);

        await _dbContext.SaveChangesAsync();

        return RedirectToAction("BookDetails", new { id = arr[1] });
    }

    [HttpGet, Authorize]
    public async Task<IActionResult> CommentsList(string id)
    {
        var commentsResult = await _dbContext.Comments
            .Include(u => u.AppUser)
            .Include(b => b.Book)
            .ThenInclude(c => c.Categories)
            .Where(p => p.AppUser.Id == id)
            .Select(s => new CommentListByAppUserIdVM
            {
                CategoryName = string.Join(" | ", s.Book.Categories.Select(ss => ss.Name).ToList()),
                Comment = s,
                BookGuid = s.Book.BookGuid.ToString()
            }).OrderByDescending(o => o.Comment.Id)
            .ToListAsync();

        return View(commentsResult);
    }
}