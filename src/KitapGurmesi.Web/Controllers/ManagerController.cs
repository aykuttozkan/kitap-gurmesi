using KitapGurmesi.Web.Models.Entities;
using KitapGurmesi.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Controllers;

[Authorize(Roles = "Admin")]
public class ManagerController : Controller
{
    private readonly ILogger<ManagerController> _logger;
    private readonly UserManager<AppUser> _userManager;
    private readonly RoleManager<AppRole> _roleManager;

    public ManagerController(ILogger<ManagerController> logger, UserManager<AppUser> userManager,
        RoleManager<AppRole> roleManager)
    {
        _logger = logger;
        _userManager = userManager;
        _roleManager = roleManager;
    }

    // GET
    public IActionResult Index()
    {
        return RedirectToAction("UserList", "Manager");
    }

    [HttpGet]
    public async Task<IActionResult> UserEdit(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            return RedirectToAction("Index");

        var userResult = await _userManager.FindByIdAsync(id);

        if (_userManager is null)
            return RedirectToAction("Index");

        ViewBag.RoleList = await _roleManager.Roles.Select(s => s.Name).ToListAsync();

        return View(new UserEditVM
        {
            Id = userResult.Id, UserName = userResult.UserName, Email = userResult.Email,
            SelectedRoles = await _userManager.GetRolesAsync(userResult)
        });
    }

    [HttpPost]
    public async Task<IActionResult> UserEdit(string id, UserEditVM model)
    {
        if (string.IsNullOrWhiteSpace(id) || model is null || string.IsNullOrWhiteSpace(model.Id))
            return RedirectToAction("Index");

        if (id != model?.Id)
            return RedirectToAction("Index");

        if (!ModelState.IsValid)
            return View(model);

        var userResult = await _userManager.FindByIdAsync(model.Id);

        if (userResult == null) return View();

        userResult.UserName = model.UserName;
        userResult.Email = model.Email;

        if (!string.IsNullOrWhiteSpace(model.Password))
        {
            if (model.Password.Length > 5)
            {
                await _userManager.RemovePasswordAsync(userResult);
                await _userManager.AddPasswordAsync(userResult, model.Password);
            }
            else
            {
                ModelState.AddModelError("", "Parola bilgisi en az 5 karakter olmalıdır.");
                return View(model);
            }
        }

        var userUpdateResult = await _userManager.UpdateAsync(userResult);

        await _userManager.RemoveFromRolesAsync(userResult, await _userManager.GetRolesAsync(userResult));
        
        if(model.SelectedRoles is { Count: > 0 })
            await _userManager.AddToRolesAsync(userResult, model.SelectedRoles);

        if (userUpdateResult.Succeeded)
            return RedirectToAction("UserList");

        foreach (var err in userUpdateResult.Errors)
        {
            ModelState.AddModelError("", err.Description);
        }

        return View(model);
    }

    [HttpGet]
    public async Task<IActionResult> UserDelete(string id)
    {
        if (string.IsNullOrWhiteSpace(id))
            return RedirectToAction("UserList");

        var userResult = await _userManager.FindByIdAsync(id);

        if (userResult is null)
            return RedirectToAction("UserList");

        var userDeletedResult = await _userManager.DeleteAsync(userResult);

        if (!userDeletedResult.Succeeded)
            _logger.LogError("Kullanıcı silinemedi", userDeletedResult.Errors.ToArray());
        else
            return RedirectToAction("UserList");

        return RedirectToAction("UserList");
    }

    [HttpGet]
    public async Task<IActionResult> UserList()
    {
        try
        {
            return View(_userManager.Users);
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Kullanıcı listesi alınırken bir hata meydana geldi. {e.Message}");
        }

        return View();
    }

    [HttpGet]
    public async Task<IActionResult> RoleList()
    {
        try
        {
            var roles = await _roleManager.Roles.ToListAsync();
            return View(roles);
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Kullanıcı listesi alınırken bir hata meydana geldi. {e.Message}");
        }

        return View();
    }

    [HttpGet]
    public IActionResult RoleAdd()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> RoleAdd(RoleAddVm model)
    {
        if (!ModelState.IsValid) return View();

        try
        {
            var role = new AppRole { Id = Guid.NewGuid().ToString(), Name = model.Name };

            var result = await _roleManager.CreateAsync(role);

            if (result.Succeeded)
                return RedirectToAction("RoleList");

            foreach (var err in result.Errors)
            {
                ModelState.AddModelError("", err.Description);
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Yeni rol eklenirken bir hata meydana geldi. {e.Message}");

            ModelState.AddModelError("", "Rol bilgisi eklenemedi. Lütfen daha sonra tekrar deneyiniz.");
        }

        return View();
    }
}