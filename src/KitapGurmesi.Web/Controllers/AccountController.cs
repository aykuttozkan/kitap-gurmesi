using System.Security.Claims;
using KitapGurmesi.Web.Models.Entities;
using KitapGurmesi.Web.ViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace KitapGurmesi.Web.Controllers;

public class AccountController : Controller
{
    private readonly ILogger<AccountController> _logger;
    private readonly UserManager<AppUser> _userManager;
    private readonly RoleManager<AppRole> _roleManager;
    private readonly SignInManager<AppUser> _signInManager;

    public AccountController(ILogger<AccountController> logger, UserManager<AppUser> userManager, RoleManager<AppRole> roleManager, SignInManager<AppUser> signInManager)
    {
        _logger = logger;
        _userManager = userManager;
        _roleManager = roleManager;
        _signInManager = signInManager;
    }
    // GET
    public IActionResult Index()
    {
        return RedirectToAction("Login");
    }

    [HttpGet]
    public IActionResult Login()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> Login(LoginVM model)
    {
        if (ModelState.IsValid)
        {
            var userFindResult = await _userManager.FindByNameAsync(model.UserName);

            if (userFindResult is null)
            {
                ModelState.AddModelError("","Hatalı kullanıcı veya parola bilgisi.");
            }
            else
            {
                var userRolesResult = await _userManager.GetRolesAsync(userFindResult);

                // giris varsa cikis yap
                await _signInManager.SignOutAsync();

                // giris yap
                var loginResult = await _signInManager.PasswordSignInAsync(userFindResult, model.Password, false, false);

                if (loginResult.Succeeded)
                {
                    //await _userManager.AddClaimAsync(userFindResult, new Claim("UserId", userFindResult.Id));
                    
                    return RedirectToAction("Index", "Profile");
                }
                
                ModelState.AddModelError("","Giriş işlemi başarısız. Lütfen tekrar deneyiniz.");
            }
        }
        
        return View();
    }

    [HttpGet]
    public IActionResult SignUp()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<IActionResult> SignUp(UserAddVM model)
    {
        if (!ModelState.IsValid)
            return View(model);

        var user = new AppUser
        {
            Id = Guid.NewGuid().ToString(),
            UserName = model.UserName,
            Email = model.Email,
            CreatedDate = DateTime.Now
        };
        
        var userCreateResult = await _userManager.CreateAsync(user, model.Password);

        if (userCreateResult.Succeeded)
        {
            var userResult = await _userManager.FindByIdAsync(user.Id);

            // role atamasını yapıyoruz
            await _userManager.AddToRoleAsync(userResult, "User");

            return RedirectToAction("Login");
        }
        
        foreach (var err in userCreateResult.Errors)
        {
            ModelState.AddModelError("", err.Description);
        }
        
        return View(model);
    }
    
    [HttpGet]
    public async Task<IActionResult> Logout()
    {
        try
        {
            // giris varsa cikis yap
            await _signInManager.SignOutAsync();
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Oturum kapatılırken bir hata meydana geldi. {e.Message}");
        }

        return RedirectToAction("Login", "Account");
    }

    [HttpGet]
    public IActionResult AccessDenied()
    {
        return View();
    }
}