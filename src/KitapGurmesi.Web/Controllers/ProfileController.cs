using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace KitapGurmesi.Web.Controllers;

[Authorize(Roles = "Admin,User,Editor")]
public class ProfileController : Controller
{
    // GET
    public IActionResult Index()
    {
        return View();
    }
}