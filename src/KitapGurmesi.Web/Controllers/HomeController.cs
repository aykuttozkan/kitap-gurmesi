﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using KitapGurmesi.Web.Models;
using KitapGurmesi.Web.Models.Contexts;
using KitapGurmesi.Web.Models.Entities;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    private readonly ApplicationDbContext _dbContext;
    
    public HomeController(ILogger<HomeController> logger, ApplicationDbContext dbContext)
    {
        _logger = logger;
        _dbContext = dbContext;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }

    [HttpGet]
    public IActionResult Demo() => Json(new { demo = "demo"});

    [HttpGet]
    public IActionResult DemoLog()
    {
        var date = DateTime.Now;
        var obj = new
        {
            message = $"Demo Log. {date}"
            
        };
        
        _logger.LogDebug($"Demo Log. {date}");
        
        return Json(obj);
    }

    [HttpGet]
    public async Task<List<AppUser>> DemoUserList(CancellationToken cancellationToken = default)
    {
        return await _dbContext.Users.ToListAsync(cancellationToken);
    }
}

