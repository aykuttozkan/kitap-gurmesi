using KitapGurmesi.Web.Models.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Models.Contexts;

public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, string>
{
    public ApplicationDbContext(DbContextOptions options) : base(options)
    {
    }

    // public DbSet<User> Users { get; set; }
    //
    // public DbSet<Role> Roles { get; set; }

    public DbSet<Author> Authors { get; set; }

    public DbSet<Category> Categories { get; set; }

    public DbSet<Book> Books { get; set; }

    public DbSet<Comment> Comments { get; set; }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        // modelBuilder.Entity<Role>().HasMany(p => p.Users).WithMany(w => w.Roles);
        // modelBuilder.Entity<User>().HasMany(p => p.Roles).WithMany(w => w.Users);
        
        // modelBuilder.Entity<User>().Property(e => e.CreatedDate)
        //     .HasDefaultValueSql("CURRENT_TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE 'Europe/Istanbul'")
        //     .ValueGeneratedOnAdd()
        //     .IsRequired();

        // modelBuilder.Entity<Comment>().HasOne(h => h.Book);
        
        base.OnModelCreating(modelBuilder);
    }
}