using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace KitapGurmesi.Web.Models.Entities;

public class AppRole : IdentityRole<string>
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int RecordId { get; set; }

    public string CreatedUserId { get; set; } = Guid.Empty.ToString();
    
    [Column(TypeName = "timestamp")]
    public DateTime CreatedDate { get; set; } = DateTime.Now;
}