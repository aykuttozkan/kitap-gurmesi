using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Models.Entities;

//[PrimaryKey(nameof(Id), nameof(CategoryGuid))]
[PrimaryKey(nameof(CategoryGuid))]
public class Category : BaseEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public Guid CategoryGuid { get; set; }
    
    public required string Name { get; set; }

    public string? Description { get; set; }
    
    public virtual ICollection<Book> Books { get; set; } = new List<Book>();
}