using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Models.Entities;

//[PrimaryKey(nameof(Id), nameof(BookGuid))]
[PrimaryKey(nameof(BookGuid))]
public class Book : BaseEntity
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public Guid BookGuid { get; set; }

    public required string Name { get; set; }

    public string Description { get; set; } = string.Empty;

    public required string EanCode { get; set; }

    public string? ImageUrl { get; set; }
    
    public Author Author { get; set; } = default!;
    
    public virtual ICollection<Category> Categories { get; set; } = new List<Category>();
    
    public virtual ICollection<Comment> Comments { get; set; }
}