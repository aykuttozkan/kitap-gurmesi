using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace KitapGurmesi.Web.Models.Entities;

public class AppUser : IdentityUser<string>
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int RecordId { get; set; }
    
    public string CreatedUserId { get; set; } = Guid.Empty.ToString();

    [Column(TypeName = "timestamp")]
    public DateTime CreatedDate { get; set; } = DateTime.Now;

    public virtual ICollection<Comment> Comments { get; set; }
}