using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KitapGurmesi.Web.Models.Entities;

public abstract class BaseEntity
{
    [Key]
    [Column(Order = 0)]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int Id { get; set; }
    
    [Column(TypeName = "timestamp")]
    public DateTime CreatedDate { get; set; }

    public Guid CreatedUserGuid { get; set; }

    [Column(TypeName = "timestamp")]
    public DateTime? UpdatedDate { get; set; }

    public Guid? UpdatedUserGuid { get; set; }

    public bool IsActive { get; set; } = true;

    public bool IsDeleted { get; set; } = false;
}