using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Models.Entities;

//[PrimaryKey(nameof(Id), nameof(AuthorGuid))]
[PrimaryKey(nameof(AuthorGuid))]
public class Author : BaseEntity
{
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public Guid AuthorGuid { get; set; }

    public required string Name { get; set; }

    public required string Surname { get; set; }

    public virtual ICollection<Book> Books { get; set; } = new List<Book>();
}