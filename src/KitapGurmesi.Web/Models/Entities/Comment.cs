using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace KitapGurmesi.Web.Models.Entities;

[PrimaryKey(nameof(CommentGuid))]
public class Comment : BaseEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    [Column(Order = 1)]
    public Guid CommentGuid { get; set; }

    public string CommentHeader { get; set; }
    
    public string CommentStr { get; set; }

    public Book Book { get; set; }

    public AppUser AppUser { get; set; }
}