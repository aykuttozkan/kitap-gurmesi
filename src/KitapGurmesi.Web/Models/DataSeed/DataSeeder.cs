using KitapGurmesi.Web.Extensions;
using KitapGurmesi.Web.Models.Contexts;
using KitapGurmesi.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Serilog;

namespace KitapGurmesi.Web.Models.DataSeed;

public static class DataSeeder
{
    public static async Task SeedData(IApplicationBuilder applicationBuilder)
    {
        using (var scope = applicationBuilder.ApplicationServices.CreateScope())
        {
            try
            {
                Log.Logger.Information($"SeedData Methodu Çalışmaya Başladı. {DateTime.Now}");

                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();

                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();

                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<AppRole>>();

                if (!(await context.Database.GetAppliedMigrationsAsync()).Any())
                    await context.Database.MigrateAsync();
                
                var firstUser = new AppUser
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "admin",
                    Email = "admin@admin.com"
                };

                var secondUser = new AppUser
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "demo",
                    Email = "demo@demo.com"
                };

                var thirdUser = new AppUser
                {
                    Id = Guid.NewGuid().ToString(),
                    UserName = "demo2",
                    Email = "demo3@demo.com"
                };

                if (!context.Users.Any())
                {
                    var firstRoleResult = await roleManager.CreateAsync(new AppRole { Id = Guid.NewGuid().ToString(), Name = "Admin" });
                    var secondRoleResult = await roleManager.CreateAsync(new AppRole { Id = Guid.NewGuid().ToString(), Name = "User" });
                    var thirdRoleResult = await roleManager.CreateAsync(new AppRole { Id = Guid.NewGuid().ToString(), Name = "Editor" });

                    // "123123".ComputeHashInLowerCase(HashingClassAlgorithms.MD5).ComputeHashInLowerCase(HashingClassAlgorithms.SHA256)
                    var firstUserResult = await userManager.CreateAsync(firstUser, "123123");
                    var secondUserResult = await userManager.CreateAsync(secondUser, "123123");
                    var thirdUserResult = await userManager.CreateAsync(thirdUser,"123123");
                    
                     var firstUserAssignRoleResult = await userManager.AddToRoleAsync(firstUser, "Admin");
                     var secondUserAssignRoleResult =await userManager.AddToRolesAsync(secondUser, new[] { "User", "Editor" });
                     var thirdUserAssignRoleResult = await userManager.AddToRoleAsync(thirdUser, "User");
                }

                if (!context.Books.Any())
                {
                    Category firstCategory = new() { Id = 1, Name = "Bilim Kurgu", CreatedDate = DateTime.Now };
                    Category secondCategory = new() { Id = 2, Name = "Kişisel Gelişim", CreatedDate = DateTime.Now };
                    Category thirdCategory = new() { Id = 3, Name = "Polisiye", CreatedDate = DateTime.Now };
                    Category fourthCategory = new() { Id = 4, Name = "Gençlik", CreatedDate = DateTime.Now };

                    Author firstAuthor = new() { Name = "Aykut", Surname = "Soyadı 1", CreatedDate = DateTime.Now };
                    Author secondAuthor = new() { Name = "Mehmet", Surname = "Soyadı 2", CreatedDate = DateTime.Now };
                    Author thirdAuthor = new() { Name = "Aslı", Surname = "Soyadı 3", CreatedDate = DateTime.Now };

                    Book firstBook = new()
                    {
                        Name = "Demo Kitap 1", EanCode = "978020137934", CreatedDate = DateTime.Now,
                        Author = firstAuthor, Categories = new List<Category>() { firstCategory, secondCategory },
                        Description = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Iaculis nunc sed augue lacus viverra vitae congue. Velit egestas dui id ornare arcu odio ut sem nulla. Nisi est sit amet facilisis magna etiam tempor orci. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Sagittis eu volutpat odio facilisis mauris sit amet massa vitae. Amet volutpat consequat mauris nunc congue nisi. Feugiat in fermentum posuere urna nec tincidunt praesent. Ac ut consequat semper viverra nam libero justo laoreet. At in tellus integer feugiat scelerisque varius. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Arcu odio ut sem nulla pharetra diam.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Iaculis nunc sed augue lacus viverra vitae congue. Velit egestas dui id ornare arcu odio ut sem nulla. Nisi est sit amet facilisis magna etiam tempor orci. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Sagittis eu volutpat odio facilisis mauris sit amet massa vitae. Amet volutpat consequat mauris nunc congue nisi. Feugiat in fermentum posuere urna nec tincidunt praesent. Ac ut consequat semper viverra nam libero justo laoreet. At in tellus integer feugiat scelerisque varius. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Arcu odio ut sem nulla pharetra diam."
                    };
                    
                    Book secondBook = new()
                    {
                        Name = "Demo Kitap 2", EanCode = "978020137906", CreatedDate = DateTime.Now,
                        Author = secondAuthor, Categories = new List<Category>() { secondCategory },
                        Description = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Iaculis nunc sed augue lacus viverra vitae congue. Velit egestas dui id ornare arcu odio ut sem nulla. Nisi est sit amet facilisis magna etiam tempor orci. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Sagittis eu volutpat odio facilisis mauris sit amet massa vitae. Amet volutpat consequat mauris nunc congue nisi. Feugiat in fermentum posuere urna nec tincidunt praesent. Ac ut consequat semper viverra nam libero justo laoreet. At in tellus integer feugiat scelerisque varius. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Arcu odio ut sem nulla pharetra diam.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Iaculis nunc sed augue lacus viverra vitae congue. Velit egestas dui id ornare arcu odio ut sem nulla. Nisi est sit amet facilisis magna etiam tempor orci. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Sagittis eu volutpat odio facilisis mauris sit amet massa vitae. Amet volutpat consequat mauris nunc congue nisi. Feugiat in fermentum posuere urna nec tincidunt praesent. Ac ut consequat semper viverra nam libero justo laoreet. At in tellus integer feugiat scelerisque varius. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Arcu odio ut sem nulla pharetra diam."
                    };
                    
                    Book thirdBook = new()
                    {
                        Name = "Demo Kitap 3", EanCode = "978020137923", CreatedDate = DateTime.Now,
                        Author = thirdAuthor, Categories = new List<Category>() { thirdCategory, fourthCategory },
                        Description = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Iaculis nunc sed augue lacus viverra vitae congue. Velit egestas dui id ornare arcu odio ut sem nulla. Nisi est sit amet facilisis magna etiam tempor orci. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Sagittis eu volutpat odio facilisis mauris sit amet massa vitae. Amet volutpat consequat mauris nunc congue nisi. Feugiat in fermentum posuere urna nec tincidunt praesent. Ac ut consequat semper viverra nam libero justo laoreet. At in tellus integer feugiat scelerisque varius. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Arcu odio ut sem nulla pharetra diam.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Consectetur adipiscing elit ut aliquam purus sit amet luctus. Iaculis nunc sed augue lacus viverra vitae congue. Velit egestas dui id ornare arcu odio ut sem nulla. Nisi est sit amet facilisis magna etiam tempor orci. Aliquam vestibulum morbi blandit cursus risus at ultrices mi. Sagittis eu volutpat odio facilisis mauris sit amet massa vitae. Amet volutpat consequat mauris nunc congue nisi. Feugiat in fermentum posuere urna nec tincidunt praesent. Ac ut consequat semper viverra nam libero justo laoreet. At in tellus integer feugiat scelerisque varius. Convallis convallis tellus id interdum velit laoreet id donec ultrices. Arcu odio ut sem nulla pharetra diam."
                    };

                    context.Books.AddRange(firstBook, secondBook, thirdBook);

                    Comment firstComment = new()
                    {
                        CommentGuid = Guid.NewGuid(),
                        CommentHeader = $"Örnek yorum başlığı. {DateTime.Now}",
                        CommentStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque convallis a cras semper auctor neque vitae tempus quam. Blandit cursus risus at ultrices mi tempus imperdiet. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Urna condimentum mattis pellentesque id nibh tortor id.",
                        Book = firstBook,
                        CreatedDate = DateTime.Now,
                        AppUser = firstUser,
                        CreatedUserGuid = Guid.Parse(firstUser.Id)
                    };
                    
                    Comment secondComment = new()
                    {
                        CommentGuid = Guid.NewGuid(),
                        CommentHeader = $"Örnek yorum başlığı. {DateTime.Now}",
                        CommentStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque convallis a cras semper auctor neque vitae tempus quam. Blandit cursus risus at ultrices mi tempus imperdiet. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Urna condimentum mattis pellentesque id nibh tortor id.",
                        Book = secondBook,
                        CreatedDate = DateTime.Now,
                        AppUser = secondUser,
                        CreatedUserGuid = Guid.Parse(secondUser.Id)
                    };
                    
                    Comment thirdComment = new()
                    {
                        CommentGuid = Guid.NewGuid(),
                        CommentHeader = $"Örnek yorum başlığı. {DateTime.Now}",
                        CommentStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque convallis a cras semper auctor neque vitae tempus quam. Blandit cursus risus at ultrices mi tempus imperdiet. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Urna condimentum mattis pellentesque id nibh tortor id.",
                        Book = thirdBook,
                        CreatedDate = DateTime.Now,
                        AppUser = thirdUser,
                        CreatedUserGuid = Guid.Parse(thirdUser.Id)
                    };
                    
                    Comment fourComment = new()
                    {
                        CommentGuid = Guid.NewGuid(),
                        CommentHeader = $"Örnek yorum başlığı. {DateTime.Now}",
                        CommentStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque convallis a cras semper auctor neque vitae tempus quam. Blandit cursus risus at ultrices mi tempus imperdiet. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Urna condimentum mattis pellentesque id nibh tortor id.",
                        Book = firstBook,
                        CreatedDate = DateTime.Now,
                        AppUser = secondUser,
                        CreatedUserGuid = Guid.Parse(secondUser.Id)
                    };
                    
                    Comment fiveComment = new()
                    {
                        CommentGuid = Guid.NewGuid(),
                        CommentHeader = $"Örnek yorum başlığı. {DateTime.Now}",
                        CommentStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Neque convallis a cras semper auctor neque vitae tempus quam. Blandit cursus risus at ultrices mi tempus imperdiet. Amet consectetur adipiscing elit duis tristique sollicitudin nibh sit. Urna condimentum mattis pellentesque id nibh tortor id.",
                        Book = firstBook,
                        CreatedDate = DateTime.Now,
                        AppUser = firstUser,
                        CreatedUserGuid = Guid.Parse(firstUser.Id)
                    };

                    await context.Comments.AddRangeAsync(firstComment, secondComment, thirdComment, fourComment, fiveComment);
                    
                    context.SaveChangesAsync().Wait();
                }

                Log.Logger.Information($"SeedData Methodu Çalışmayı Tamamladı. {DateTime.Now}");
            }
            catch (Exception e)
            {
                Log.Error(e, $"SeedData Methodu Çalışması Sırasında Bir Hata İle Karşılaştı. {e.Message}");
            }
        }
    }
}