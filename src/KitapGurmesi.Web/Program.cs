﻿using KitapGurmesi.Web.Models.Contexts;
using KitapGurmesi.Web.Models.DataSeed;
using KitapGurmesi.Web.Models.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using NpgsqlTypes;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.PostgreSQL.ColumnWriters;
using Serilog.Sinks.PostgreSQL;
using Serilog.Ui.PostgreSqlProvider;
using Serilog.Ui.Web;

var builder = WebApplication.CreateBuilder(args);

#region ...: SeriLog :...
// http-s://github.com/serilog-contrib/Serilog.Sinks.Postgresql.Alternative/issues/50
//Serilog.Debugging.SelfLog.Enable(msg => Console.WriteLine(msg));
//Serilog.Debugging.SelfLog.Enable(Console.Error);

var logTableName = "logs";

IDictionary<string, ColumnWriterBase> columnWriters = new Dictionary<string, ColumnWriterBase>
{
    { "message", new RenderedMessageColumnWriter(NpgsqlDbType.Text) },
    { "message_template", new MessageTemplateColumnWriter(NpgsqlDbType.Text) },
    { "level_str", new LevelColumnWriter(true, NpgsqlDbType.Varchar) },
    { "level", new LevelColumnWriter(false) },
    { "raise_date", new TimestampColumnWriter(NpgsqlDbType.TimestampTz) },
    { "timestamp", new TimestampColumnWriter(NpgsqlDbType.Timestamp)},
    { "exception", new ExceptionColumnWriter(NpgsqlDbType.Text) },
    { "properties", new LogEventSerializedColumnWriter(NpgsqlDbType.Jsonb) },
    //{ "props_test", new PropertiesColumnWriter(NpgsqlDbType.Jsonb) },
    { "log_event", new PropertiesColumnWriter(NpgsqlDbType.Jsonb) },
    { "machine_name", new SinglePropertyColumnWriter("MachineName", PropertyWriteMethod.ToString, NpgsqlDbType.Text, "l") }
};

// https://stackoverflow.com/questions/58038428/turn-off-mvc-request-logging-in-asp-net-core-2-2-with-serilog
// request akisini loglamayi devre disi biraktik
Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
    .MinimumLevel.Override("System", LogEventLevel.Error)
    .WriteTo.PostgreSQL(builder.Configuration.GetConnectionString("KitapGurmesi"), logTableName, columnWriters, needAutoCreateTable: true, schemaName: "public")
    .WriteTo.Console()
    .CreateLogger();

builder.Host.UseSerilog();

builder.Services.AddSerilogUi(options => 
    options.UseNpgSql(builder.Configuration.GetConnectionString("KitapGurmesi"), logTableName)
);

Log.Logger.Information("System Start");
#endregion

builder.Services.AddDbContext<ApplicationDbContext>(opt =>
{
    opt.UseNpgsql(builder.Configuration.GetConnectionString("KitapGurmesi"));
    opt.EnableSensitiveDataLogging();
});

#region ...: Identity Ayarlari :...
builder.Services.AddIdentity<AppUser, AppRole>().AddEntityFrameworkStores<ApplicationDbContext>();

builder.Services.Configure<IdentityOptions>(options =>
{
    options.Password.RequiredLength = 6;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequireLowercase = false;
    options.Password.RequireUppercase = false;
    options.Password.RequireDigit = false;

    options.User.RequireUniqueEmail = true;
});
#endregion

// Add services to the container.
// builder.Services.AddControllersWithViews().AddJsonOptions(options =>
// {
//     options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
//     options.JsonSerializerOptions.WriteIndented = true;
// });

builder.Services.AddControllersWithViews().AddNewtonsoftJson(options =>
{
    options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    // options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.All;
});

builder.Services.ConfigureApplicationCookie(options =>
{
    options.LoginPath = "/Account/Login";
    options.LogoutPath = "/Account/Logout";
    options.AccessDeniedPath = "/Account/AccessDenied";
    options.SlidingExpiration = false;
    options.ExpireTimeSpan = TimeSpan.FromHours(8);
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseSerilogUi(options =>
{
    options.RoutePrefix = "serilog-ui";
    options.HomeUrl = "/#Logs";
    options.InjectJavascript("/js/serilog-ui/custom.js");
});

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

// using (var scope = app.Services.CreateScope())
// {
//     var dbContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
//     DataSeeder.SeedData(dbContext);
// }

DataSeeder.SeedData(app).Wait();

app.Run();

